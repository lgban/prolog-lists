% factorial(0, 1).
% factorial(N, F) :- N > 0, N1 is N - 1, factorial(N1, F1), F is N * F1.

longitud([], 0).
longitud([_|T], Longitud) :- longitud(T, LongitudPrime), Longitud is LongitudPrime + 1.

% ?- longitud([3,2,5,7], X).