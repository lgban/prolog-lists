arc(g, h).
arc(g, d).
arc(e, d).
arc(h, f).
arc(e, f).
arc(a, e).
arc(a, b).
arc(b, f).
arc(b, c).
arc(f, c).


% path(X, Y) :- X \= Y, arc(X, Y).
% path(X, Y) :- arc(X, Z), path(Z, Y).

path(X, Y, PathPrime) :- path(X, Y, [X], Path), reverse(Path, PathPrime).

path(X, Y, Visited, Path) :- arc(X, Y), Path = [Y|Visited].
path(X, Y, Visited, Path) :- arc(X, Z), path(Z, Y, [Z|Visited], Path).
