longitud2(L, N) :- accumulate(L, 0, N).

accumulate([], A, A).
accumulate([_|T], A, N) :-
    A1 is A + 1,
    accumulate(T, A1, N).

% ?- longitud([4,2,6,1,4,5], N).